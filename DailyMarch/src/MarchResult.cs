﻿namespace Taijj.DailyMarch
{
    class MarchResult
    {
        public MarchResult(int day, int distance)
        {
            Day = day;
            Distance = distance;
        }

        public string ToPrettyString()
        {            
            return string.Format("{0}. Tag: {1} km", Day, Distance);
        }

        public int Day { get; private set; }
        public int Distance { get; private set; }
    }
}