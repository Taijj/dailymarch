﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace Taijj.DailyMarch
{
    class DailyMarch
    {
        #region Main
        static void Main(string[] args)
        {
            Reader = new InputFileReader();
            Results = new List<MarchResult>();

            while(true)
            {
                if(false == TryReadInput())
                {
                    Console.WriteLine("Data read from file is not valid. Try again?");
                    Console.WriteLine();
                    continue;
                }

                CalculateResults();
                OutputResults();
                Console.WriteLine();
            }
        }

        private static InputFileReader Reader { get; set; }
        private static List<MarchResult> Results { get; set; }
        #endregion




        #region Input Reading
        private static bool TryReadInput()
        {
            string input = HandleConsoleInput();
            Reader.ReadInputFromFile(input);
            return Reader.InputIsValid;
        }

        private static string HandleConsoleInput()
        {
            string input = "";
            while(string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Enter input file name (without extension):");
                input = Console.ReadLine();
            }

            if(input == "exit")
            {
                Environment.Exit(0);
            }
            return input;
        }
        #endregion



        #region Result Calculation
        private static void CalculateResults()
        {
            int bestGuess = CalculateBestGuess();
            int stops = int.MaxValue;
            while(stops >= Reader.DayCount)
            {
                Results.Clear();
                stops = CalculateStopsAndCacheResults(bestGuess);                
                bestGuess++;
            }            
        }

        private static int CalculateBestGuess()
        {
            int totalDistance = Reader.Segments.Sum();
            int maxDistance = Reader.Segments.Max();
            int guess = (int)Math.Ceiling((double)totalDistance / (double)Reader.DayCount);
            return maxDistance > guess ? maxDistance : guess;
        }

        private static int CalculateStopsAndCacheResults(int bestGuess)
        {
            int stops = 0;
            int traveledDistance = 0;
            for(int i = 0; i < Reader.SegmentCount; i++)
            {
                int currentDistance = Reader.Segments[i];
                traveledDistance += currentDistance;

                if(traveledDistance > bestGuess)
                {
                    stops++;
                    Results.Add(new MarchResult(stops, traveledDistance - currentDistance));
                    traveledDistance = currentDistance;
                }

                if(i == Reader.SegmentCount - 1)
                {
                    Results.Add(new MarchResult(stops+1, traveledDistance));
                }
            }
            return stops;
        }
        #endregion



        #region Output
        private static void OutputResults()
        {
            Results.ForEach(r => Console.WriteLine(r.ToPrettyString()));
            Console.WriteLine();

            string maxOutput = string.Format("Maximum: {0} km", Results.Max(r => r.Distance)); 
            Console.WriteLine(maxOutput);
        }
        #endregion

    }
}
