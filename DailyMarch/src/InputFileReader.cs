﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Text;

namespace Taijj.DailyMarch
{
    class InputFileReader
    {
        #region Init
        const string DEFAULT_INPUT_FILE = @"\default.txt";
        const int UNASSIGNED_VALUE = -1;

        public InputFileReader()
        {
            RelativeApplicationPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            Segments = new List<int>();
        }
        #endregion
        


        #region Read
        public void ReadInputFromFile(string fileName)
        {
            StringBuilder builder = new StringBuilder(RelativeApplicationPath);
            builder.Append(@"\");
            builder.Append(fileName);
            builder.Append(".txt");

            string filePath = SanitizeFilePath(builder.ToString());
            ReadFromFileAt(filePath);         
        }

        private string SanitizeFilePath(string filePath)
        {
            if(false == File.Exists(filePath))
            {
                string message = string.Format("File at path {0} does not exist! Default path is used!", filePath);
                Console.WriteLine(message);
                return RelativeApplicationPath + DEFAULT_INPUT_FILE;
            }
            return filePath;
        }

        private void ReadFromFileAt(string filePath)
        {
            SegmentCount = UNASSIGNED_VALUE;
            DayCount = UNASSIGNED_VALUE;
            Segments.Clear();
            using(TextReader reader = File.OpenText(filePath))
            {
                while(true)
                {
                    string line = reader.ReadLine();
                    if(string.IsNullOrEmpty(line))
                    {
                        break;
                    }
                    TryReadFrom(line);
                }
            }
        }

        private void TryReadFrom(string line)
        {
            if(SegmentCountIsUnassigned)
            {
                SegmentCount = TryParseToInt(line);
            }
            else if(DayCountIsUnassigned)
            {
                DayCount = TryParseToInt(line);
            }
            else
            {
                Segments.Add(TryParseToInt(line));
            }
        }

        private int TryParseToInt(string line)
        {
            int value = UNASSIGNED_VALUE;
            int.TryParse(line, out value);
            return value;
        }
                
        private bool SegmentCountIsUnassigned { get { return SegmentCount == UNASSIGNED_VALUE; } }
        private bool DayCountIsUnassigned { get { return DayCount == UNASSIGNED_VALUE; } }
        #endregion



        #region Validation
        public bool InputIsValid
        {
            get
            {
                return SegmentCount > 0
                    && DayCount > 0
                    && SegmentCount >= DayCount
                    && Segments.Count > 0
                    && Segments.Count >= DayCount
                    && Segments.Count == SegmentCount
                    && Segments.All(i => i > 0);
            }
        }
        #endregion



        #region Properties
        private string RelativeApplicationPath { get; set; }

        public int SegmentCount { get; private set; }
        public int DayCount { get; private set; }
        public List<int> Segments { get; private set; }
        #endregion
    }
}
